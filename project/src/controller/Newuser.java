package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Userdao;
import model.User;


/**
 * Servlet implementation class Newuser
 */
@WebServlet("/Newuser")
public class Newuser extends HttpServlet {


    /**
     * @see HttpServlet#HttpServlet()
     */
    public Newuser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// ユーザ一覧のサーブレットにリダイレクト

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User u = (User)session.getAttribute("userInfo");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new_user.jsp");
		dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		Userdao userdao = new Userdao();

		String loginId = request.getParameter("loginid");
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		User user = userdao.findByLoginId(loginId);

		if(user!=null||loginId.equals("")||name.equals("")||password.equals("")||password1.equals("")||birthDate.equals("")||!password.equals(password1)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			request.setAttribute("loginid", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new_user.jsp");
			dispatcher.forward(request, response);
			return;
		}

		userdao.createUser(loginId, password, name, birthDate);
		response.sendRedirect("UserListServlet");
	}
}
