package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Userdao;
import model.User;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User u = (User)session.getAttribute("userInfo");

		if (u == null) {

			response.sendRedirect("LoginServlet");
			return;
		}

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		Userdao userdao = new Userdao();
		User user = userdao.userRefe(Integer.parseInt(id));

		request.setAttribute("userrefe", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
		dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		Userdao userdao = new Userdao();

		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String id = request.getParameter("id");

		Userdao userDao = new Userdao();
		User user = userDao.userRefe(Integer.parseInt(id));

		if(name.equals("")||birthDate.equals("")||!password.equals(password1)) {

			request.setAttribute("userrefe", user);
			request.setAttribute("errMsg", "「入力された内容は正しくありません」");

			request.setAttribute("name", request.getParameter("name"));
			request.setAttribute("birthDate", request.getParameter("birthDate"));

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(password.equals("")){
			userdao.userUpDate(name, birthDate, id);

		}else {
			userdao.userUpdate(password,name,birthDate,id);
		}

		response.sendRedirect("UserListServlet");
	}

}
