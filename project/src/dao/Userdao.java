package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class Userdao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, userenc(password));
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}
			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE login_id!='admin'";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void createUser(String loginId, String password, String name, String birthDate) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String insql = "INSERT INTO user(login_id,password,name,birth_date,create_date,update_date)VALUES(?,?,?,?,now(),now())";

				PreparedStatement pStmt = conn.prepareStatement(insql);

				pStmt.setString(1, loginId);
				pStmt.setString(2, userenc(password));
				pStmt.setString(3, name);
				pStmt.setString(4, birthDate);

				pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

public void userUpdate(String password, String name, String birthDate, String id) {
	Connection conn = null;

	try {

		conn = DBManager.getConnection();

		String upsql = "UPDATE user SET password=?,name=?,birth_date=?,update_date=now() WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(upsql);

			pStmt.setString(1, userenc(password));
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, id);

			pStmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}

public void userUpDate(String name, String birthDate,String id) {
	Connection conn = null;

	try {
		conn = DBManager.getConnection();

		String upsql = "UPDATE user SET name=?,birth_date=?,update_date=now() WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(upsql);

			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, id);

			pStmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}

public void userDelete(int id) {
	Connection conn = null;

	try {
		conn = DBManager.getConnection();

		String delsql = "DELETE FROM user WHERE id =?";

			PreparedStatement pStmt = conn.prepareStatement(delsql);

			pStmt.setInt(1, id);

			pStmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

				}
			}
		}
	}

public User userRefe(int id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int Id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			return new User(Id,loginId,name,birthDate,password,createDate,updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

public List<User> userfind(String loginId,String name,String startDate,String endDate) {
	Connection conn = null;
	List<User> userList = new ArrayList<User>();

	try {

		conn = DBManager.getConnection();

		String sql = "SELECT * FROM user WHERE login_id !='admin'";

		if(!loginId.equals("")) {
			sql+=" AND login_id ='"+loginId+"'";
		}

		if(!name.equals("")) {
			sql+=" AND name like '%"+name+"%'";
		}

		if(!startDate.equals("")) {
			sql+=" AND birth_date >='"+startDate+"'";
		}

		if(!endDate.equals("")) {
			sql+=" AND birth_date <='"+endDate+"'";
		}

		Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

		while (rs.next()) {
			int id = rs.getInt("id");
			String LoginId = rs.getString("login_id");
			String Name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(id, LoginId, Name, birthDate, password, createDate, updateDate);

			userList.add(user);

		}
	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;

				}
			}
		}
	return userList;
	}

public String userenc(String password) {

	String source = password;
	Charset charset = StandardCharsets.UTF_8;
	String algorithm = "MD5";

	byte[] bytes = null;
	try {
		bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	} catch (NoSuchAlgorithmException e) {

		e.printStackTrace();
	}
	String result = DatatypeConverter.printHexBinary(bytes);
	System.out.print(result);

	return result;

	}

public User findByLoginId(String loginId) {
	Connection conn = null;
	try {
		conn = DBManager.getConnection();

		String sql = "SELECT * FROM user WHERE login_id = ?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, loginId);

		ResultSet rs = pStmt.executeQuery();

		// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
		if (!rs.next()) {
			return null;
		}

		// 必要なデータのみインスタンスのフィールドに追加
		String loginIdData = rs.getString("login_id");

		return new User(loginIdData);

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}
}