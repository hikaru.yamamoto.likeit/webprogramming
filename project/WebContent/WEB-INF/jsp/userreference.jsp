<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>情報詳細参照</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="inb"> ${userInfo.name}さん <a href="Logout">  <span style="color:red">ログアウト</span></a></div>

    <div class="inner">
        <h1>ユーザ情報詳細参照</h1>

<form method="post" action="UserReference">
<table class="noborder" align="center" >
<tr>
<th class="noborder" >ログインID</th>
<td class="noborder" >${userrefe.loginId}</td>
</tr>
<tr>
<th class="noborder" >ユーザ名</th>
<td class="noborder"> ${userrefe.name}</td>
</tr>
<tr>
<th class="noborder" >生年月日</th>
<td class="noborder" >${userrefe.birthDate}</td>
<tr>
<th class="noborder" >登録日時</th>
<td class="noborder" >${userrefe.createDate}</td>
<tr>
<th class="noborder" >更新日時</th>
<td class="noborder" >${userrefe.updateDate}</td>
</tr>
</table>
</form>
    </div>

    <br>
    <a href="UserListServlet" ><span style="color:blue">戻る</span></a>

</body>
</html>