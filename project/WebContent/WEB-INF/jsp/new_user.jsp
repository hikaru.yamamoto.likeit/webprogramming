<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>新規登録</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="row">
        <div class="inb">  ${userInfo.name}さん <a href="Logout">  <span style="color:red">ログアウト</span></a></div></div>
    <div class="inner">
    <h1>ユーザー新規登録</h1>
	<form action="Newuser" method="post">
    <table class="noborder" align="center" >
<tr>
<th class="noborder" >ログインID</th>
<td class="noborder" ><input type="text" name="loginid" style="width:200px;"value="${loginid}"></td>
</tr>
<tr>
<th class="noborder" >パスワード</th>
<td class="noborder" ><input type="text" name="password" style="width:200px;"></td>
</tr>
<tr>
<th class="noborder" >パスワード（確認）</th>
<td class="noborder" ><input type="text" name="password1" style="width:200px;"></td>
<tr>
<th class="noborder" >ユーザ名</th>
<td class="noborder" ><input type="text" name="name" style="width:200px;"value="${name}"></td>
<tr>
<th class="noborder" >生年月日</th>
<td class="noborder" ><input type="text" name="birthDate" style="width:200px;"value="${birthDate}"></td>
</tr>
</table>
 <c:if test="${errMsg != null}" ><div class="alert alert-danger" role="alert" style="color:red">${errMsg}</div></c:if>
    <input type="submit" value="登録">
    </form>
    </div>
    <br>
    <a href="UserListServlet" ><span style="color:blue">戻る</span></a>
</body>
</html>