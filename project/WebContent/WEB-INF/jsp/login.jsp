<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン画面</title>
        <link href="style.css" rel="stylesheet" type="text/css" />
</head>
	<body>

            <div class="inner">

            <h1>ログイン画面</h1>

            <c:if test="${errMsg != null}" ><div class="alert alert-danger"  style="color:red" role="alert">${errMsg}</div></c:if>


             <form class="form-signin" action="LoginServlet" method="post">

       <p>ログインID <input type="text" name="loginId" style="width:200px" id="inputLoginId"  placeholder="ログインID" required autofocus > </p>



      <p>パスワード <input type="password" name="password" style="width:200px" id="inputPassword"  placeholder="パスワード" required ></p>


           <p> <input type="submit" value="ログイン"> </p>

         </form>
       </div>
	</body>
</html>