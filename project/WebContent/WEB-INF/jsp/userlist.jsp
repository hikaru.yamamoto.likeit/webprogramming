<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ一覧</title>
        <link href="style.css" rel="stylesheet" type="text/css" />
</head>
	<body>
         <div class="row">
        <div class="inb">  ${userInfo.name} さん <a href="Logout"><span style="color:red">ログアウト</span></a></div></div>

            <div class="inner">

            <h1>ユーザ一覧</h1>

                <div class="right"> <a href="Newuser"><span style="color:blue">新規登録</span></a></div>


<form method="post" action="UserListServlet">
<table class="noborder" align="center" >
<tr>
<th class="noborder" >ログインID</th>
<td class="noborder" ><input type="text" name="loginId" style="width:220px;"></td>
<tr>
<th class="noborder" >ユーザ名</th>
<td class="noborder" ><input type="text" name="name"  style="width:220px;"></td>
<tr>
<th class="noborder" >生年月日</th>
<td class="noborder" ><input type="date" style="width:150px;"  name="startDate" >～
    <input type="date" style="width:150px;" name="endDate"></td>
</tr>
</table>

        <div class="row">
            <div class="right"><input type="submit" style="width:10%;padding:5px;font-size:20px;" value="検索" > </div>
          <hr></div>
</form>

<table align="center">
    <tr>
      <th class="s1-1">ログインID</th>
      <th class="s1-1">ユーザ名</th>
      <th class="s1-1">生年月日</th>
      <th class="s1-1"></th>
    </tr>
     <c:forEach var="user" items="${userList}" >
    <tr>
      <td>${user.loginId}</td>
      <td>${user.name}</td>
      <td>${user.birthDate}</td>
      <td>
      <c:if test="${userInfo.loginId=='admin'}">
      <a href="UserReference?id=${user.id}"><input type="submit" class="btn" value="詳細" ></a>
   	  <a href="UserUpdate?id=${user.id}"><input type="submit" class="btn1" value="更新"></a>
   	  <a href="UserDelete?id=${user.id}"><input type="submit" class="btn2" value="削除"></a></c:if>

   	  <c:if test="${userInfo.loginId!='admin'}">
   	  <a href="UserReference?id=${user.id}"><input type="submit" class="btn" value="詳細" ></a>

   	  <c:if test="${userInfo.loginId==user.loginId}">
   	  <a href="UserUpdate?id=${user.id}"><input type="submit" class="btn1" value="更新"></a></c:if></c:if>
        </td>
    </tr>
    </c:forEach>
  </table>
     </div>
	</body>
</html>
